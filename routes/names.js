var express = require('express');
var router = express.Router();
var Name = require('../models/name')



router.post('/addname',function(req,res,next)
{
    var name = new Name({name: req.body.name , phonenumber: req.body.phonenumber,  emailaddress:req.body.emailaddress, password:req.body.password, confirmpassword:req.body.confirmpassword});
    
    name.save(function(err, docs)
    {
        if(err) return handleError(err);
        res.send('success')
    });
})

router.post('/deleteone',function(req,res,next)
{
    Name.findOneAndDelete({_id:req.body.id}, function(err,docs)
    {
        if(err)
        {
            res.send('error occured')
        }
        else
        {
            res.send('deleted')
        }
    })
})
router.post('/updatename' , function(req, res, next)
{
  Name.findOneAndUpdate(req.body.name , {$set:{name:'Krishna'}} , function(err, doc){
    if(err)
    {
      res.send('error occured')
    }
    else
    {
      res.send('updated')
    }
 })
})

router.post('/loguser', function(req,res,next){
    Name.find({emailaddress:req.body.emailaddress},{password:req.body.password}, function(err,docs){
        if (!docs) 
        {
            res.status(500).send(err);
        } 
        else if (!docs) 
        {
            res.send('must enter details');
        }
        else 
        {
            res.status(200).send(docs);
        } 
     })
})
 module.exports = router;

