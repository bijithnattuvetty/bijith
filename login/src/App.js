import React, { Component } from 'react';
import './App.css';
import Registration from './Registration';
import Login from './Login';
import {BrowserRouter as Router,Link,Switch,Route} from "react-router-dom";
    
      class App extends Component 
      {
        render()
         {
          return (
            <Router>
                  <Switch>
                     <Route exact path="/Registration" component={Registration} />
                     <Route path="/" component={Login} />
                 </Switch>
            </Router>
       
          );
         }
      }
      
      export default App;

